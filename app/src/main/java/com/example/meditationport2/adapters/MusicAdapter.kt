package com.example.meditationport2.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.meditationport2.MusicItem
import com.example.meditationport2.R

class MusicAdapter(val context:Context, val list: ArrayList<MusicItem> ): RecyclerView.Adapter<MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val root = LayoutInflater.from(context).inflate(R.layout.music_adapter, parent,false)
        return MyViewHolder(root)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.grouplabel.text = list[position].grouplabel
        holder.songlabel.text = list[position].songlabel
        holder.poster.setImageResource(list[position].poster)

    }

    override fun getItemCount(): Int {
       return list.size
    }
}

class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val grouplabel:TextView = itemView.findViewById(R.id.group_label)
    val songlabel:TextView = itemView.findViewById(R.id.song_label)
    val poster:ImageView = itemView.findViewById(R.id.poster)

}
