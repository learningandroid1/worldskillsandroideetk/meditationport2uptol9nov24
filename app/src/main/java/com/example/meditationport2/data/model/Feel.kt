package com.example.meditationport2.data.model

data class Feel(val id:Int, val title:String, val image:String)
