package com.example.meditationport2.data.model

data class LoginResponse(
    val id: String,
    val email: String,
    val nickName: String,
    val avatar: String,
    val token: String
)
