package com.example.meditationport2.data.model

class Quote(
   val id: Int,
   val title: String,
   val image: String,
   val description: String)
