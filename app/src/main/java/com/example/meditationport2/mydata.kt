package com.example.meditationport2

data class feel(val image:Int, val name:String)
class MyFeel{val list = arrayListOf(feel(R.drawable.ic_relax, "Расслабленным"),
feel(R.drawable.ic_calm___icon, "Спокойным"),
feel(R.drawable.ic_focus, "Сосредоточенным"),
feel(R.drawable.ic_calm___icon, "Спокойным"),
feel(R.drawable.ic_relax, "Расслабленным"),
feel(R.drawable.ic_relax, "Расслабленным"),
feel(R.drawable.ic_relax, "Расслабленным"),
feel(R.drawable.ic_relax, "Расслабленным"),
feel(R.drawable.ic_relax, "Расслабленным")
)}

data class state(val title:String, val descr:String, val image_state:Int)
class MyState{
    val list2 = arrayListOf(state("Статья 1", "Описание к статье 1", R.drawable.image1),
    state("Статья 2", "Описание к статье 2", R.drawable.image2),
    state("Статья 3", "Описание к статье 3", R.drawable.image1),
    state("Статья 4", "Описание к статье 4", R.drawable.image2))
}

data class MusicItem(val grouplabel:String, val songlabel:String, val poster:Int)
class MyMusic{
    val list= arrayListOf(
        MusicItem("Group1", "Song1", R.drawable.maxresdefault),
        MusicItem("Group3", "Song3", R.drawable.maxresdefault),
        MusicItem("Group2", "Song2", R.drawable.maxresdefault),
    )
}