package com.example.meditationport2.ui.home

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.meditationport2.MyFeel
import com.example.meditationport2.MyState
import com.example.meditationport2.R
import com.example.meditationport2.adapters.FeelAdapter
import com.example.meditationport2.adapters.StateAdapter
import com.example.meditationport2.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        val avatar: ImageView = root.findViewById(R.id.avatar)
        val hellotext: TextView = root.findViewById(R.id.helloText)
        val sharedPreferences:SharedPreferences? = activity?.getSharedPreferences("main", Context.MODE_PRIVATE)
        Glide.with(requireContext()).load(sharedPreferences?.getString("avatar", "")).into(avatar)

        val recyclerView: RecyclerView = root.findViewById(R.id.feel)
        val recyclerView2: RecyclerView = root.findViewById(R.id.state)
        recyclerView.adapter = FeelAdapter(requireContext(), MyFeel().list)
        recyclerView2.adapter = StateAdapter(requireContext(),MyState().list2)

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}