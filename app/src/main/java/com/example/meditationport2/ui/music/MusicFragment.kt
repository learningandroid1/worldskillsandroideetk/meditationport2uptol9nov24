package com.example.meditationport2.ui.music

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.example.meditationport2.MyMusic
import com.example.meditationport2.R
import com.example.meditationport2.adapters.MusicAdapter

class MusicFragment : Fragment(R.layout.fragment_music) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewPager:ViewPager2 = view.findViewById(R.id.view_pager)
        viewPager.adapter = MusicAdapter(requireContext(),MyMusic().list)
    }

}