package com.example.meditationport2.ui.profile

import android.content.Context.MODE_PRIVATE
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.example.meditationport2.R
import com.example.meditationport2.SignInActivity

class ProfileFragment : Fragment(R.layout.fragment_profile) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val button: Button = view.findViewById(R.id.exit)
        val sharedPreferences: SharedPreferences? = activity?.getSharedPreferences("main", MODE_PRIVATE)
            button.setOnClickListener {
            if(sharedPreferences != null){
            val editor = sharedPreferences.edit()
            editor.putString("token", null)
            editor.apply()
            }
            val intent = Intent(requireContext(), SignInActivity::class.java)
                activity?.startActivity(intent)

        }
    }
}